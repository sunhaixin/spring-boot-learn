package springbootspike.response;

public class CommonReturnType {
    //表明对应请求返回的处理结果 "success" 或 "fail"
    private String status;

    //若status为success的话，data正常返回json数据
    //若status为fail的话，data返回对应的错误状态码
    private Object data;

    //定义一个通用的创建方法
    public static CommonReturnType create(Object result){
            return CommonReturnType.create(result,"success");
    }

    public static CommonReturnType create(Object result,String status){
        CommonReturnType type = new CommonReturnType();
        type.setData(result);
        type.setStatus(status);
        return type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
