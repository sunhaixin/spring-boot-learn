package springbootspike.error;

/**
 * Created by sidney on 2019/04/03
 */

public interface CommonError {
    public int getErrCode();
    public String getErrMsg();
    public CommonError setErrMsg(String errMsg);
}
