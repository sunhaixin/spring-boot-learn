package springbootspike.service.model;

import java.math.BigDecimal;

/**
 * Created by sidney on 2019/4/12 2:27 PM
 * The name package is springbootspike.service.model
 */
//用户下单的交易模型
public class OrderModel {
    //2018102100012828
    private String id;

    //购买用户的id
    private Integer userId;

    //购买商品的id
    private Integer itemId;

    //购买商品的单价
    private BigDecimal itemPrice;

    //购买的数量
    private Integer amount;

    //购买的金额
    private BigDecimal orderPrice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getItemId() {
        return itemId;
    }

    public void setItemId(Integer itemId) {
        this.itemId = itemId;
    }

    public BigDecimal getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(BigDecimal itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public BigDecimal getOrderPrice() {
        return orderPrice;
    }

    public void setOrderPrice(BigDecimal orderPrice) {
        this.orderPrice = orderPrice;
    }

}