package springbootspike.service;

import springbootspike.service.model.PromoModel;

/**
 * Created by sidney on 2019/5/5 3:48 PM
 * The name package is springbootspike.service
 */
public interface PromoService {
    //根据itemId获取即将的或正在进行的秒杀活动
    PromoModel getPromoByItemId(Integer itemId);
}
