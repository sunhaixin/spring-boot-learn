package springbootspike.service;

import springbootspike.error.BusinessException;
import springbootspike.service.model.OrderModel;

/**
 * Created by sidney on 2019/4/12 3:33 PM
 * The name package is springbootspike.service
 */
public interface OrderService {
    public OrderModel createOrder(Integer userId, Integer itemId, Integer amount) throws BusinessException;
}
