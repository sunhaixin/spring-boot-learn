package springbootspike;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springbootspike.dao.UserDOMapper;
import springbootspike.dataobject.UserDO;

/**
 * Hello world!
 *
 */
@SuppressWarnings("ALL")
@SpringBootApplication(scanBasePackages = {"springbootspike"})
@RestController

@MapperScan("springbootspike.dao")
public class App 
{
    /**
     *
     */
    @Autowired
    private UserDOMapper userDOMapper;

    @RequestMapping("/")
    public String Home(){
        UserDO userDO = userDOMapper.selectByPrimaryKey(1);
        if(userDO == null){
            return "用户对象不存在";
        }else {
            return userDO.getName();
        }
    }

    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        SpringApplication.run(App.class, args);
    }
}
// Tomcat started on port(s): 8080 (http) with context path ''
