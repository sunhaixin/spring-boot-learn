package springbootspike.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import springbootspike.error.BusinessException;
import springbootspike.error.EmBusinessError;
import springbootspike.response.CommonReturnType;
import springbootspike.service.OrderService;
import springbootspike.service.model.OrderModel;
import springbootspike.service.model.UserModel;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by sidney on 2019/4/13 1:02 PM
 * The name package is springbootspike.controller
 */
@Controller("/order")
@RequestMapping("/order")
@CrossOrigin(allowCredentials = "true",allowedHeaders = "*")
public class OrderController extends BaseController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private HttpServletRequest httpServletRequest;

    //封装下单请求
    @RequestMapping(value = "/createOrder",method = RequestMethod.POST,consumes = {CONTENT_TYPE_FORMED})
    @ResponseBody
    public CommonReturnType createOrder(
            @RequestParam(name = "itemId")Integer itemId,
            @RequestParam(name = "amount")Integer amount) throws BusinessException {
        Boolean isLogin = (Boolean) httpServletRequest.getSession().getAttribute("IS_LOGIN");
        if(isLogin == null){
            throw new BusinessException(EmBusinessError.PARAMETER_VALIDATION_ERROR,"用户还未登录不能下单");
        }
        //获取用户登录信息
        UserModel userModel = (UserModel)httpServletRequest.getSession().getAttribute("LOGIN_USER");
        OrderModel orderModel = orderService.createOrder(userModel.getId(),itemId,amount);;
        return CommonReturnType.create(null);

    }
}
