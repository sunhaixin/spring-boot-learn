[English](./README.en.md) | 简体中文

<h1 align="center">spring-boot-learn</h1>

<div align="center">

SpringBoot是整合所有的框架,并通过一行简单的main方法启动应用

![](./spring.jpg)

</div>

#### 介绍
  * spring-boot-sendmail: 
    * SpringBoot发送邮件 

  * spring-boot-spike: 
    * SpringBoot构建电商基础秒杀项目
  
## 参与贡献

- [sunhaixin](https://gitee.com/github-29425276/spring-boot-learn)