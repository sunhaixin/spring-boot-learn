[简体中文](./README.md) | English

# spring-boot-sendmail

#### Description
利用 Spring Boot 发送邮件

#### Contribution

- [sunhaixin](https://gitee.com/github-29425276/spring-boot-sendmail)

#### Summary

  * learnSummary

    1. [独立微服务](#独立微服务)
    2. [异常处理](#异常处理)
    3. [定时重试邮件](#定时重试邮件)
    4. [异步发送](#异步发送)

  * wholeSummary

    1. [邮件发送的历史和原理](#邮件发送的历史和原理)
    2. [SpringBoot和邮件系统](#SpringBoot和邮件系统)
    3. [各类型邮件的发送](#各类型邮件的发送)
    4. [邮件系统](#邮件系统)

