[English](./README.en.md) | 简体中文

# spring-boot-sendmail

#### 介绍
利用 Spring Boot 发送邮件

## 参与贡献

- [sunhaixin](https://gitee.com/github-29425276/spring-boot-sendmail)

#### 总结

  * 学习总结

    1. [独立微服务](#独立微服务)
    2. [异常处理](#异常处理)
    3. [定时重试邮件](#定时重试邮件)
    4. [异步发送](#异步发送)

  * 整体总结

    1. [邮件发送的历史和原理](#邮件发送的历史和原理)
    2. [SpringBoot和邮件系统](#SpringBoot和邮件系统)
    3. [各类型邮件的发送](#各类型邮件的发送)
    4. [邮件系统](#邮件系统)

