package com.neo.HelloWorld.seivice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
@Service

public class MailService {


    @Value("${spring.mail.username}")
    private String from;

    @Autowired
    private JavaMailSender mailSender;

    // 打印Hello World
    public void sayHello(){
        System.out.println("Hello World");
    }

    // 文本邮件
    public void sendSimpleMail(String to,String subject,String content){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(content);
        message.setFrom(from);
        mailSender.send(message);
    }

    // HTML邮件
    public void sendHtmlMail(String to,String subject,String content) {
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message,true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content,true);
            helper.setFrom(from);
            mailSender.send(message);
            System.out.println("发送HTML邮件成功");
        } catch (MessagingException e) {
            System.out.println("发送HTML邮件失败");
        }
    }

    // 附件邮件
    public void sendAttachmentsMail(String to,String subject,String content, String filePath){
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message,true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content,true);
            helper.setFrom(from);

            FileSystemResource file = new FileSystemResource(new File(filePath));
            String fileName = file.getFilename();
            helper.addAttachment(fileName,file);
            /* helper.addAttachment(fileName +"_test",file); */
            mailSender.send(message);
            System.out.println("发送附件邮件成功");
        } catch (MessagingException e) {
            System.out.println("发送附件邮件失败");
        }

    }

    //图片邮件
    public void sendInlinResourceMail(String to,String subject,String content,String rscPath,String rscId) {
        MimeMessage message = mailSender.createMimeMessage();

        MimeMessageHelper helper = null;
        try {
            helper = new MimeMessageHelper(message,true);
            helper.setTo(to);
            helper.setSubject(subject);
            helper.setText(content,true);
            helper.setFrom(from);

            FileSystemResource res = new FileSystemResource(new File(rscPath));
            helper.addInline(rscId,res);
            mailSender.send(message);
            System.out.println("发送图片邮件成功");
        } catch (MessagingException e) {
            System.out.println("发送图片邮件失败");
        }
    }
}
