package com.neo.HelloWorld.service;

import com.neo.HelloWorld.seivice.MailService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import javax.mail.MessagingException;

@RunWith(SpringRunner.class)
@SpringBootTest

public class ServiceTest {
    @Resource
    MailService mailService;
    @Test
    public void  sayHelloTest(){
        mailService.sayHello();
    }

    @Test
    public void  sendSimpleMailTest(){//文本发送
        mailService.sendSimpleMail("sunhaixin@daosh.com","这是第一份邮件","搭建好了这是我的第一份邮件！");
    }
    @Test
    public void  snedHtmlMailTest() {//html发送
        String content = "<html>\n" + "<body>\n" + "<h3> hello world, 这是一份HTML文件！" + "</body>\n" + "</html>";
        mailService.sendHtmlMail("sunhaixin@daosh.com", "这是第一份邮件", content);
    }
    @Test
    public void  sendAttachmentsMailTest(){//附件发送
        String filePath = "//Users/fuzhiguang/clone/spring-boot-sendmail/spring-boot-mail.zip";
        String content = "<html>\n" + "<body>\n" + "<h3>这是一份带有附件的文件！" + "</body>\n" + "</html>";
        mailService.sendAttachmentsMail("sunhaixin@daosh.com","这是第一份邮件",content, filePath);
    }
    @Test
    public void  sendInlinResourceMailTest(){//图片发送
        String imgPath = "//Users/fuzhiguang/显卡天梯图.jpg";
        String rscId ="neo001";
        String content = "<html><body>这是有图片的邮件:<img src=\'cid:"+ rscId + "\'></img></body></html>";
        mailService.sendInlinResourceMail("liyuan@daosh.com","这是第一份邮件",content, imgPath, rscId);

    }
}
